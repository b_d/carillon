<!DOCTYPE html>
<html lang="en">
<head>
	<title>Carillion</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
    
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/main.js"></script>     

<script type="text/javascript" src="//www.google.fr/jsapi"></script>
<script type="text/javascript">
    google.load("maps", "3.4", {
    	other_params: "sensor=false&language=en"
    });
</script>
<script type="text/javascript" src="js/jquery.googlemap.js"></script>
    
  <link rel="stylesheet" href="css/styles.css" />
    
</head>
<body class="location">

   <?php 
        if(isset($_GET['l']))   
        {
            include('php/'.$_GET['l'].'.php');    
        }
        else
        {
            include('php/it.php');    
        }
        
    ?>
    <?php 
        $src = 'location';
        include('php/header.php');
    ?>


            <div id="map" style="width: 100%; height:500px;"></div>
    
    <?php include('php/footer.php');?>
    
<script type="text/javascript">  
    
  $(function() {
      
      var x = $(window).height()   - 180;    
      var div = document.getElementById('map');
      div.setAttribute("style","height:"+x+"px; width:100%;");
      $("#map").googleMap({
        zoom: 19, // Initial zoom level (optional)
      coords: [42.097396, 11.8003707], // Map center (optional)
      });
      $("#map").addMarker({
      coords: [42.097396, 11.8003707], // GPS coords
      title: 'Carillon', // Title
      text:  '<div style="width:150px;height:50px;text-align: right;">Via Antonio Canova 4d,</br>00053 Civitavecchia (RM)</div>' // HTML content
    });
  })
</script>
    
    
    
  <script src="js/jquery-1.8.2.min.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/scripts.js"></script>
  <script>
    $(document).ready(function(){
     $('#gallery-container').sGallery({
        fullScreenEnabled: true
      });

    });

  </script>
</body>
</html>