<!DOCTYPE html>
<html lang="en">
<head>
	<title>Carillion</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

    
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/main.js"></script>    
 
  <link rel="stylesheet" href="css/styles.css" />
  <link rel="stylesheet" href="css/forma.css" />
    
</head>
<body>

   <?php 
        if(isset($_GET['l']))   
        {
            include('php/'.$_GET['l'].'.php');    
        }
        else
        {
            include('php/it.php');    
        }
        
    ?>
    <?php 
        $src = 'contact';
        include('php/header.php');
    ?>


	<section class="listings"  align="center" style="padding:20px 0;">
        <div id="mojDiv" style="height:600px">
            
                        <p style="text-align:center; margin-bottom:10px;
    font-family: 'Source Sans Pro', sans-serif;
    font-size:18px;
    color:#fff;"> <?php 
        if(isset($_GET['s']))   
        {
            echo $jezik->mailPoslan .'</br>'; 
        }
         echo $jezik->podrska . '</br>0-24
                </br></br>E-mail: <a href="mailto:carillon2015@gmail.com"> carillon2015@gmail.com </a>
                </br>'.$jezik->adresa .': Via Antonio Canova 4d, 00053 Civitavecchia (RM)
                </br>'. $jezik->telefon . ': 0039 328 945 94 27
                </br>';
            ?>
            </p>
            <br>
            <br>
        <form action="php/sendMail.php" method="post">
            <p style="text-align:left; margin-bottom:10px;
    font-family: 'Source Sans Pro', sans-serif;
    font-size:18px;
    color:#fff;">
                <?php echo $jezik->posaljiNamEmail; ?>
            </p>
            
            <input name="name" placeholder="<?php echo $jezik->vaseIme; ?>" class="name" required />
            <input name="emailaddress" placeholder="<?php echo $jezik->vasEmail; ?>" class="email" type="email" required />
            <textarea rows="4" cols="50" name="subject" placeholder="<?php echo $jezik->vasaPoruka; ?>" class="message" required></textarea>
            <input name="l"  type="hidden" value="<?php
                        if(!isset($_GET['l']))   
                        {
                            echo '0"';
                        }
                        else
                        {
                            echo $_GET['l'];
                        }
        
                   ?>"/>
            <input name="submit" class="btn" type="submit" value="<?php echo $jezik->posalji; ?>" />
        </form>
        </div>
	</section>	<!--  


end listing section  -->
    
    
    <?php include('php/footer.php');?>
  
<script type="text/javascript">  
    
  $(function() {
      var x = $(window).height();    
      if(x>800)
      {
          var div = document.getElementById('mojDiv');
          div.setAttribute("style","height:"+(x-272)+"px");      
      }      
       
  })
</script>
</body>
</html>