<!DOCTYPE html>
<html lang="en">
<head>
	<title>Carillion</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/main.js"></script>      
  <link rel="stylesheet" href="css/styles.css" />
    
</head>
<body>

   <?php 
        if(isset($_GET['l']))   
        {
            include('php/'.$_GET['l'].'.php');    
        }
        else
        {
            include('php/it.php');    
        }
        
        $src = 'room';
        $id = $_GET['id'];
        include('php/header.php');


    if($_GET['id'] == '1')
    {
        echo '
    <section class="listings"  align="center" style="padding:20px 0;">
<div id="gallery-container">
    
    <ul class="items--small">
      <li class="item"><a href="#"><img src="img/s1/1.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s1/2.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s1/3.JPG" alt=""/></a></li>
      <li class="item"><a href="#"><img src="img/s1/4.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s1/5.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s1/6.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s1/7.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s1/8.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s1/9.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s1/10.JPG" alt="" /></a></li>

    </ul>
    <ul class="items--big">
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/1.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/2.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/3.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/4.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/5.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>

      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/6.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/7.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/8.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/9.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/10.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>

    </ul>
    <div class="controls">
      <span class="control icon-arrow-left" data-direction="previous"></span> 
      <span class="control icon-arrow-right" data-direction="next"></span> 
      <span class="grid icon-grid"></span>
      <span class="fs-toggle icon-fullscreen"></span>   
    </div>
    
  </div>
	</section>	<!--  end listing section  -->
    
<div class="mojDiv">

		<div class="about" style="font-size:18px;">
			<h3>'.$jezik->soba1 .'</h3>
			<p>'.$jezik->soba1Opis.'
    </p>
		</div>

	</div>';
    }
else if($_GET['id'] == '2')
{
    echo '
        <section class="listings"  align="center" style="padding:20px 0;">
<div id="gallery-container">
    
    <ul class="items--small">
      <li class="item"><a href="#"><img src="img/s2/1.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s2/2.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s2/3.JPG" alt=""/></a></li>
      <li class="item"><a href="#"><img src="img/s2/4.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s2/5.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s2/6.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s2/7.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s2/8.JPG" alt="" /></a></li>

    </ul>
    <ul class="items--big">
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s2/1.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s2/2.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s2/3.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s2/4.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s2/5.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>

      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s1/6.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s2/7.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s2/8.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      
    </ul>
    <div class="controls">
      <span class="control icon-arrow-left" data-direction="previous"></span> 
      <span class="control icon-arrow-right" data-direction="next"></span> 
      <span class="grid icon-grid"></span>
      <span class="fs-toggle icon-fullscreen"></span>   
    </div>
    
  </div>
	</section>	<!--  end listing section  -->
    
<div class="mojDiv">

		<div class="about" style="font-size:18px;">
			<h3>'.$jezik->soba2 .'</h3>
			<p>'.$jezik->soba2Opis.'
    </p>
		</div>

	</div>';
}
else if($_GET['id'] == '3')
{
        echo '
        <section class="listings"  align="center" style="padding:20px 0;">
<div id="gallery-container">
    
    <ul class="items--small">
      <li class="item"><a href="#"><img src="img/s3/1.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s3/2.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s3/3.JPG" alt=""/></a></li>
      <li class="item"><a href="#"><img src="img/s3/4.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s3/5.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s3/6.JPG" alt="" /></a></li>
      <li class="item"><a href="#"><img src="img/s3/7.JPG" alt="" /></a></li>

    </ul>
    <ul class="items--big">
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s3/1.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s3/2.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s3/3.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s3/4.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s3/5.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>

      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s3/6.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      <li class="item--big">
        <a href="#">
          <figure>
            <img src="img/s3/7.JPG" alt="" />
            <figcaption class="img-caption">
              Caption
            </figcaption>
          </figure>
          </a>
      </li>
      
      
    </ul>
    <div class="controls">
      <span class="control icon-arrow-left" data-direction="previous"></span> 
      <span class="control icon-arrow-right" data-direction="next"></span> 
      <span class="grid icon-grid"></span>
      <span class="fs-toggle icon-fullscreen"></span>   
    </div>
    
  </div>
	</section>	<!--  end listing section  -->
    
<div class="mojDiv">

		<div class="about" style="font-size:18px;">
			<h3>'.$jezik->soba3 .'</h3>
			<p>'.$jezik->soba3Opis.'
    </p>
		</div>

	</div>';
}
    
    
	
    
    include('php/footer.php');
    
    ?>
  <script src="js/jquery-1.8.2.min.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/scripts.js"></script>
  <script>
    $(document).ready(function(){
     $('#gallery-container').sGallery({
        fullScreenEnabled: true
      });
    });
  </script>
</body>
</html>