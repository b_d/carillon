<?php
    $jezik = new StdClass;
    $jezik->sobe = 'Sobe';
    $jezik->lokacija= 'Lokacija';
    $jezik->kontakt = 'Kontakt';
    $jezik->onama = 'Početna';
    $jezik->footerNote = 'Poruka';
    $jezik->svaPrava = 'Sva prava zadržana.';
    $jezik->izradio = 'Izradio: ';
    $jezik->mailPoslan = 'Vas e-mail je uspješno poslan!';
    $jezik->podrska = 'KORISNIČKA PODRŠKA';
    $jezik->telefon = 'Telefon';
    $jezik->posaljiNamEmail   = 'Pošaljite nam e-mail!';
    $jezik->vaseIme = 'Vaše ime?';
    $jezik->vasEmail = 'Vaš e-mail?';
    $jezik->vasaPoruka = 'Molimo, unesite poruku';
    $jezik->posalji = 'Pošalji';

    $jezik->sobeGeneralno = '</br>Gostima nudimo na raspolaganje odnedavno potpuno restauriran apartman koji se sastoji od 4 spavaće sobe.</br> Svaka soba posjeduje kupatilo sa wc-om.</br>Stilski opremljene sobe pružaju topao i opuštajući ugođaj, obilje sunčevog svjetla, obojene veselim tonovima i modernim namjestajem omogućit će vam da se uistinu osjećate kao da ste u vlastitom domu.</br> Apartman posjeduje i kuhinju (dnevni boravak) kao zajednici prostor sviju gostiju namjenjen za posluzivanje dorucka.';
    $jezik->soba1 = 'Soba Split';
    $jezik->soba1Kratko = 'Bračna soba';
    $jezik->soba2Kratko = 'Bračna soba';
    $jezik->soba3Kratko = 'Bračna soba';
    $jezik->soba1Opis = 'Udobna i prostrana soba je opremljena sljedecim sadržajima: </br>● TV</br>● Klima uredjaj</br>● Wi-Fi </br>● Kupatilo sa WC-om</br>● Posteljinom i šugomanima </br>● Anatomskim i udobnim madracima i jastucima.</br>Postoji mogucnost dodatka 3. kreveta.';
    $jezik->soba2 = 'Soba Roma';
    $jezik->soba2Opis = 'Takodjer prostrana soba, opremljena sljedecim sadržajima: </br>● TV </br>● Klima uredjaj </br>● Wi-Fi </br>● Kupatilo sa WC-om</br>● Posteljinom i šugomanima </br>● Anatomskim i udobnim madracima i jastucima';
    $jezik->soba3 = 'Soba Firenze';
    $jezik->soba3Opis = 'Elegancija je naglasak u ovoj posebno uredjenoj sobi. </br>Opremljena je sljedećim sadržajima: </br>● TV</br>● Klima uređjaj</br>● Wi-Fi </br>● Kupatilo sa WC-om</br>● Posteljinom i šugomanima </br>● Anatomskim i udobnim madracima i jastucima.</br>Soba ima veliki i prostrani balkon opremljen stolom i stolicama.</br>Postoji mogućnost dodatka 3.kreveta u nadoplatu od 15 % od ukupne cijene sobe.';
$jezik->adresa = 'Adresa';


    $jezik->apartmani = "Bed and breakfast CARILLON je idealan za vaš odmor ukoliko želite boraviti u mirnom i opuštenom okruženju.</br> Nalazi se u blizini centra grada, izoliran od buke i gradskog prometa te pruža jedinstven pogled na lijepo uređen vrt u mediteranskom stilu.</br></br> Svojom strateškom pozicijom osigurava nam dostupnost svih potrebnih sadržaja: </br>● udaljenost od luke 800 m </br>● udaljenost od željeznicke stanice 1000 m</br>●  bolnica 100 m</br>● gradska vijećnica 700 m</br>● udaljenost od mora i  gradske šetnice 800 m</br>U zoni u kojoj se nalazi u okruženju od 100 m možete naći trgovine, ljekarne, pizzerije, slastičarnice, restorane i barove.</br> Za sve goste koji dolaze osobnim automobilom na raspolaganju je i besplatni parking u sklopu stambene zgrade.</br> U neposrednoj blizini nalazi se i autocesta koja vas vodi na ulaz u grad Rim.";
    $jezik->sredisnjiTekst = "ZAŠTO ČEKATI? </br>DOĐITE I OTKRIJTE ČARI OVOG ROMANTIČNOG AMBIJENTA  U SRCU CIVITAVECCHIJE.";
?>