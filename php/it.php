<?php
    $jezik = new StdClass;
    $jezik->sobe = 'Camere';
    $jezik->lokacija= 'Posizione';
    $jezik->kontakt = 'Contatto';
    $jezik->onama = 'Casa';
    $jezik->footerNote = 'Messaggio';
    $jezik->svaPrava = 'Tutti i diritti riservati.';
    $jezik->izradio = 'Sviluppato: ';
    $jezik->mailPoslan = 'La tua email è stata inviata!';
    $jezik->podrska = 'SERVIZIO CLIENTI';
    $jezik->telefon = 'Telefono';
    $jezik->posaljiNamEmail   = 'Inviaci una e-mail!';
    $jezik->vaseIme = 'Il tuo nome?';
    $jezik->vasEmail = 'La tua email?';
    $jezik->vasaPoruka = 'Inserisci un messaggio';
    $jezik->posalji = 'Inviare';


    $jezik->apartmani = "Il Bed and Breakfast Carillon rappresenta un angolo di relax metropolitano nella città di Civitavecchia.</br> Situato in zona semicentrale, l’appartamento si affaccia su uno splendido e curato cortile.</br></br> Rappresenta un punto strategico e logistico in quanto nel raggio di 1 chilometro si trovano tutti i punti di maggiore interesse o necessità: </br>● 800 metri dal porto </br>● 1000 metri dalla stazione ferroviaria</br>● 100 metri dall’ospedale</br>● 100 metri dal Tribunale e 700 metri dal centro cittadino e dallo splendido lungomare “La Marina”. </br>Nel  tranquillo quartiere avrete tutti i servizi necessari, dal discount alla farmacia, dal bar pasticceria alla pizzeria, dal ristorante al negozio di prodotti elettronici. </br>Inoltre, per chi ci raggiungerà con le proprie automobili, vi è a disposizione un parcheggio interno condominiale oppure parcheggi esterni totalmente gratuiti.";
    $jezik->sredisnjiTekst = "E allora che cosa aspettate!!! </br>
Venite a scoprire questo romantico e rilassante B&B nel cuore di Civitavecchia
";

 $jezik->sobeGeneralno = "</br>Gli ospiti vengono accolti in un appartamento recentemente ristrutturato, arredato con forme e colori caldi, tra la dimensione privata  dell'abitare ed un ospitalità autentica. </br>L’appartamento è dotato dei principali servizi e comfort ed è costituito da un ampio soggiorno con balcone che si affaccia su un cortile sempreverde, da una cucina, semplice ed essenziale dotata di frigorifero, e da tre camere spaziose ed accoglienti";
    $jezik->soba1 = 'Camera Split';
    $jezik->soba1Kratko = 'Camera matrimoniale';
    $jezik->soba2Kratko = 'Camera matrimoniale';
    $jezik->soba3Kratko = 'Camera matrimoniale';
    $jezik->soba1Opis = 'La camera Split, spaziosa e confortevole, è dotata di: </br>● TV </br>● Aria condizionata</br>● WiFi</br>● Bagno privato con box doccia. </br>● Si fornisce asciugamano e telo doccia </br>● I materassi sono realizzati con oltre 800 molle insacchettate garantendo un riposo rilassante e benefico </br>● I cuscini in dotazione sono anallergici ed anatomici.</br>Arredata con mobili in pino chiaro e con colori freschi e rilassanti.</br>Possibilità di aggiungere il terzo letto con un sovraprezzo del 15%.';
    $jezik->soba2 = 'Camera Roma';
    $jezik->soba2Opis = 'La camera Roma, spaziosa e confortevole, è dotata di: </br>● TV</br>● Aria condizionata</br>● WiFi</br>● Bagno esclusivo esterno con box doccia</br>● Si fornisce asciugamano e telo doccia</br>● I materassi sono realizzati con oltre 800 molle insacchettate garantendo un riposo rilassante e benefico</br>● I cuscini in dotazione sono anallergici ed anatomici. </br>Arredata con mobili in legno bianco antico presenta la testata del letto in ecopelle bordeaux trasmettendo un senso di grandezza e sicurezza';

    $jezik->soba3 = 'Camera Firenze';
    $jezik->soba3Opis = 'La camera Firenze, elegante e spaziosa, è il fiore all’occhiello del Bed And Breakfast Carillon.</br> E’ dotata di: </br>● TV</br>● Aria condizionata</br>● WiFi</br>● Bagno privato con box doccia ed un ampio balcone dove vivere momenti di piacevole relax</br>● Si fornisce asciugamano e telo doccia</br>● I materassi sono realizzati con oltre 800 molle insacchettate garantendo un riposo rilassante e benefico</br>● I cuscini in dotazione sono anallergici ed anatomici. </br>Arredata con mobili in legno noce, presenta la testata del letto in ecopelle tortora che ben si sposa con la pittura bordeaux ed i decorativi stucchi del soffitto, </br>dando agli ospiti un caldo tocco di romanticismo e raffinatezza. </br>Possibilità di aggiungere il terzo letto con un sovraprezzo del 15%';
$jezik->adresa = 'Indirizzo';

?>