<?php
    $jezik = new StdClass;
    $jezik->sobe = 'Rooms';
    $jezik->lokacija= 'Location';
    $jezik->kontakt = 'Contact';
    $jezik->onama = 'Home';
    $jezik->footerNote = 'Message';
    $jezik->svaPrava = 'All rights reserved.';
    $jezik->izradio = 'Made by: ';
    $jezik->mailPoslan = 'Your e-mail is sent!';
    $jezik->podrska = 'CUSTOMER SUPPORT';
    $jezik->telefon = 'Telephone';
    $jezik->posaljiNamEmail   = 'Send us an e-mail!';
    $jezik->vaseIme = 'What is your name?';
    $jezik->vasEmail = 'What is your e-mail?';
    $jezik->vasaPoruka = 'Please enter your message';
    $jezik->posalji = 'Send';

    $jezik->sobeGeneralno = "</br> We offer recently fully restored apartment, which consists of 4 bedrooms. </br> Each room has a bathroom with toilet. </br> The stylish rooms provide a warm and relaxing atmosphere with plenty of sunlight and modern furniture that will allow you to really feel like you're in your own home. </br> The apartment has a kitchen (living room) as a community space designed for all guests for breakfast.";
    $jezik->soba1 = 'Apartment Split';
    $jezik->soba1Kratko = 'Master bedroom';
    $jezik->soba2Kratko = 'Master bedroom';
    $jezik->soba3Kratko = 'Master bedroom';
    $jezik->soba1Opis = 'Comfortable and spacious room, it is equipped with: </br> ● TV </br> ● Air conditioning </br> ● Wi-Fi </br> ● Bathroom with toilet </br> ● Linen and towels </br> ● Anatomic and comfortable mattresses and pillows. </br> There is the possibility of the addition of the third bed.';
    $jezik->soba2 = 'Apartment Roma';
    $jezik->soba2Opis = 'Also spacious room, equipped with </br> ● TV </br> ● Air conditioning </br> ● Wi-Fi </br> ● Bathroom with toilet </br> ● Linen and towels </br> ● Anatomic and comfortable mattresses and pillows';
    $jezik->soba3 = 'Apartment Firenze';
    $jezik->soba3Opis = 'Elegance is the emphasis of this specially arranged room. </br> It is equipped with: </br> ● TV </br> ● Air conditioning </br> ● Wi-Fi </br> ● Bathroom with toilet </br> ● Linen and towels </br> ● Anatomic and comfortable mattresses and pillows. </br> The room has a spacious balcony with table and chairs. </br> There is a possibility to supplement 3rd bed  with the payment of 15% of the total price of the room.';
$jezik->adresa = 'Adresa';


    $jezik->apartmani = "Bed and breakfast CARILLON is ideal for your holiday if you want to stay in a calm and relaxed environment. </br> It is located near the center of the city, isolated from the noise and city traffic and offers a unique view of the lovely garden in the Mediterranean style. </br> </br> With its strategic position it provides you access to all necessary facilities: </br> ● distance from port 800 m </br> ● distance from the train station 1000 m </br> ● hospital 100 m </br> ● city Hall 700 m </br> ● distance from the sea and promenade 800 m </br> in the zone of 100 m from the apartment you can find shops, pharmacies, pizzerias, pastry shops, restaurants and bars. </br> For all guests arriving by car there is also free parking as part of the building. </br> In the vicinity there is a highway that takes you to the entrance of the city of Rome.";
    $jezik->sredisnjiTekst = "WHY WAIT? </br> COME AND DISCOVER THE MAGIC OF THIS ROMANTIC ENVIRONMENT IN THE HEART CIVITAVECCHIA.";
?>