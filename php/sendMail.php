<?php
/**
 * This example shows sending a message using PHP's mail() function.
 */

require 'mailer.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Set who the message is to be sent from
$mail->setFrom('carillon@carillon.com', 'Webmaster');
//Set an alternative reply-to address
$mail->addReplyTo('replyto@example.com', 'First Last');
//Set who the message is to be sent to
$mail->addAddress('carillon2015@gmail.com');
//Set the subject line
$mail->Subject = 'Poruka sa Carillon web-a';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$emailAdresa = $_POST['emailaddress'];
$ime = $_POST['name'];
$poruka = $_POST['subject'];


$mail->msgHTML('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>PHPMailer Test</title>
</head>
<body>
<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;">
  <h1>Mail: </h1><p>'.$emailAdresa.'</p>
  </br><h1>Ime: </h1><p>'.$ime.'</p>
  </br><h1>Poruka: </h1><p>'.$poruka.'</p>
</div>
</body>
</html>');

//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}

$l = $_POST['l'];
if($l=='0')
    $dodatno = '?s=1';
else
    $dodatno = '?s=1&l=' . $l;
header('Location: ../contact.php'.$dodatno);
?>