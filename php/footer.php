<?php
    echo '
    <footer>
		<div class="wrapper footer">
			<ul>
				
				<li class="about">
					<p>' .$jezik->footerNote . '</p>
					<ul>
						<li><a href="http://facebook.com/pixelhint" class="facebook" target="_blank"></a></li>
						<li><a href="http://twitter.com/pixelhint" class="twitter" target="_blank"></a></li>
						<li><a href="http://plus.google.com/+Pixelhint" class="google" target="_blank"></a></li>
						<li><a href="#" class="skype"></a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="copyrights wrapper">
			Copyright © 2015 Carillon. ' .$jezik->svaPrava . '
            </br>' .$jezik->izradio . ' Dino
		</div>
	</footer><!--  end footer  -->
    ';
?>