<!DOCTYPE html>
<html lang="en">
<head>
	<title>Carillion</title>
	<meta charset="utf-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
        
  <link rel="stylesheet" href="css/stylesRooms.css" />
</head>
<body class="rooms">
    

   <?php 
        if(isset($_GET['l']))   
        {
            include('php/'.$_GET['l'].'.php');    
        }
        else
        {
            include('php/it.php');    
        }
        
    ?>
    <?php 
        $src = 'rooms';
        include('php/header.php');
    ?>
                  <div id='opis' style="text-align:center;font-family: 'Source Sans Pro', sans-serif; font-size:16px;  color:#fff;">
                      <?php echo $jezik->sobeGeneralno;
            ?>
            </div>
    
    <script type="text/javascript">  
    
  $(function() {
      
      var x = $(window).height() * 0.07;    
      //alert(x);
      var div = document.getElementById('opis');
      div.setAttribute("style","padding-left:"+x+"px; padding-right:"+x+"px;text-align:center;font-family: 'Source Sans Pro', sans-serif; font-size:20px;  color:#fff;");     
    });
</script>
    
	<section class="listings">
		<div class="wrapper">
			<ul id='mojaLista' class="properties_list" style="
    padding-left: 0px
">
				
				<li>
					<a href="room.php?id=1<?php if(isset($_GET['l'])){ echo '&l='.$_GET['l'];}?>">
						<img src="img/s1/1.JPG" alt="" title="" class="property_img"/>
					</a>
					<div class="property_details">
						<h1>
							<a href="room.php?id=1<?php if(isset($_GET['l'])){ echo '&l='.$_GET['l'];}?>"><?php echo $jezik->soba1; ?></a>
						</h1>
                        <h2><?php echo $jezik->soba1Kratko; ?></h2>
					</div>
				</li>
				<li>
					<a href="room.php?id=2<?php if(isset($_GET['l'])){ echo '&l='.$_GET['l'];}?>">
						<img src="img/s2/1.JPG" alt="" title="" class="property_img"/>
					</a>
					<div class="property_details">
						<h1>
							<a href="room.php?id=2<?php if(isset($_GET['l'])){ echo '&l='.$_GET['l'];}?>"><?php echo $jezik->soba2; ?></a>
						</h1>
						<h2><?php echo $jezik->soba2Kratko; ?></h2>
					</div>
				</li>
				<li style="
                        margin-right: 40px;
                    ">
					<a href="room.php?id=3<?php if(isset($_GET['l'])){ echo '&l='.$_GET['l'];}?>">
						<img src="img/s3/1.JPG" alt="" title="" class="property_img"/>
					</a>
					<div class="property_details">
						<h1>
							<a href="room.php?id=3<?php if(isset($_GET['l'])){ echo '&l='.$_GET['l'];}?>"><?php echo $jezik->soba3; ?></a>
						</h1>
						<h2><?php echo $jezik->soba3Kratko; ?></h2>
					</div>
				</li>
			</ul>
		</div>
	</section>	<!--  end listing section  -->
    
    
    <?php include('php/footer.php');?>
	<script type="text/javascript">      
      $(function() {      
          var x = $(window).width();    
          if(x>800)
          {           
              var div = document.getElementById('mojaLista');
              div.setAttribute("style","padding-left:"+(x/10)+"px");   
          }
      })
</script>
    
</body>
</html>